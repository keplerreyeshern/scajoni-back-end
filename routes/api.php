<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\DataController;
use App\Http\Controllers\Api\CategoryController;
use App\Http\Controllers\Api\PasswordResetController;
use App\Http\Controllers\Api\ProductController;
use App\Http\Controllers\Api\SaucerController;
use App\Http\Controllers\Api\UserController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});

Route::get('/data-public', [DataController::class, 'public']);


// Password
Route::get('/password/find/{token}', [PasswordResetController::class, 'find']);
Route::post('/password/create', [PasswordResetController::class, 'create']);
Route::post('/password/reset', [PasswordResetController::class, 'reset']);

Route::group(['middleware' => ['auth:api']], function(){
    // User
    Route::get('/user', function (Request $request) {
        return $request->user();
    });
    // Data
    Route::get('/data-admin', [DataController::class, 'admin']);

    // Users
    Route::resource('/users', UserController::class);

    // Categories
    Route::resource('/categories', CategoryController::class);
    Route::post('/categories/{category}', [CategoryController::class, 'update']);

    // Products
    Route::resource('/products', ProductController::class);
    Route::post('/products/{product}', [ProductController::class, 'update']);

    // Saucers
    Route::resource('/saucers', SaucerController::class);
    Route::post('/saucers/{saucer}', [SaucerController::class, 'update']);
});
