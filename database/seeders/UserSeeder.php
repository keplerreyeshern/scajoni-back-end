<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name' => 'Kepler Reyes Hernandez',
            'email' => 'keplerreyeshern@gmail.com',
            'password' => Hash::make('Balam.0412'),
            'telephone' => '7228905403',
            'profile' => 'admin',
        ]);
        User::create([
            'name' => 'Scarlett Gregorio Reyes',
            'email' => 'scarlett@gmail.com',
            'password' => Hash::make('1234567890'),
            'telephone' => '7293621044',
            'profile' => 'admin',
        ]);
        User::create([
            'name' => 'Jocsan Gregorio Reyes',
            'email' => 'joc.gregoreyes@gmail.com',
            'password' => Hash::make('1234567890'),
            'telephone' => '7291575814',
            'profile' => 'admin',
        ]);
        User::create([
            'name' => 'Quetzaly Gregorio Reyes',
            'email' => 'quetzaly@gmail.com',
            'password' => Hash::make('1234567890'),
            'telephone' => '7228905903',
            'profile' => 'admin',
        ]);
    }
}
