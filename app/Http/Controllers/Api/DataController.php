<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Prescription;
use App\Models\PrescriptionProduct;
use App\Models\Product;
use App\Models\Saucer;
use App\Models\User;
use Illuminate\Http\Request;

class DataController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function admin()
    {
        $users = User::all();
        $categories = Category::get();
        $products = Product::get();
        $saucers = Saucer::get();
        $prescriptions = Prescription::get();
        $prescription_products = PrescriptionProduct::get();
        $result = [
            'users' => $users,
            'categories' => $categories,
            'products' => $products,
            'saucers' => $saucers,
            'prescriptions' => $prescriptions,
            'prescription_products' => $prescription_products
        ];
        return $result;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function public()
    {
        $categories = Category::get();
        $saucers = Saucer::get();
        $result = [
            'categories' => $categories,
            'saucers' => $saucers,
        ];
        return $result;
    }
}
