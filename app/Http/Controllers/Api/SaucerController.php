<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Prescription;
use App\Models\PrescriptionProduct;
use App\Models\Saucer;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str as Str;

class SaucerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $saucer = new Saucer();
        $saucer->name = $request['name'];
        $saucer->slug = Str::slug($request['name']);
        $saucer->description = $request['description'];
        $saucer->price = $request['price'];
        $saucer->category_id = $request['category'];
        $file = $request->file('image');
        if ($file){
            $name =  time()."_".$file->getClientOriginalName();
            Storage::disk('public')->put('/images/saucers/' . $name,  \File::get($file));
            $saucer->image = '/storage/images/saucers/' . $name;
        }
        $saucer->active = true;
        $saucer->save();

        $prescription = new Prescription();
        $prescription->name = 'Receta de ' . $request['name'];
        $prescription->slug = Str::slug('Receta de ' . $request['name']);
        $prescription->saucer_id = $saucer->id;
        $prescription->save();

        $products = json_decode($request['products'], true);
        foreach ($products as $product){
            $prescriptionProduct = new PrescriptionProduct();
            $prescriptionProduct->prescription_id = $prescription->id;
            $prescriptionProduct->product_id = $product['id'];
            $prescriptionProduct->amount = $product['amount'];
            $prescriptionProduct->save();
        }

        return $saucer;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $saucer = Saucer::findOrFail($id);
        if($saucer->new){
            $saucer->new = false;
        } else {
            $saucer->new = true;
        }
        $saucer->save();

        return $saucer;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $saucer = Saucer::findOrFail($id);
        if($saucer->active){
            $saucer->active = false;
        } else {
            $saucer->active = true;
        }
        $saucer->save();

        return $saucer;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $saucer = Saucer::findOrFail($id);
        $saucer->name = $request['name'];
        $saucer->slug = Str::slug($request['name']);
        $saucer->description = $request['description'];
        $saucer->price = $request['price'];
        $saucer->category_id = $request['category'];
        $file = $request->file('image');
        if ($file){
            $name =  time()."_".$file->getClientOriginalName();
            Storage::disk('public')->put('/images/saucers/' . $name,  \File::get($file));
            $saucer->image = '/storage/images/saucers/' . $name;
        }
        $saucer->active = true;
        $saucer->save();

        $prescription = Prescription::where('saucer_id', $saucer->id)->first();
        $prescription->name = 'Receta de ' . $request['name'];
        $prescription->slug = Str::slug('Receta de ' . $request['name']);
        $prescription->save();

        $products = json_decode($request['products'], true);
        foreach ($products as $product){
            $prescriptionProduct = PrescriptionProduct::where('prescription_id', $prescription->id)
                ->where('product_id', $product['id'])->first();
            $prescriptionProduct->amount = $product['amount'];
            $prescriptionProduct->save();
        }

        return $saucer;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $saucer = Saucer::findOrFail($id);
        $saucer->delete();

        return $saucer;
    }
}
