<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PrescriptionProduct extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = 'prescription_products';

    protected $fillable = ['prescription_id', 'product_id'];
}
