<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Saucer extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = 'saucers';

    protected $fillable = ['name', 'slug', 'description', 'price', 'active', 'category_id'];
}
